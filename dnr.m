classdef dnr
    % Simulate the settling particles using an analog Drop-aNd-Roll
    % algorithm, following the idea of Jodrey and Tory (1985). Some
    % implementations are naive, and can be further improved.
    %
    % In dealing with the touching of particles, we define the touching
    % threshold dmin as:
    % dmin = min(r) / 100
    % so that particles pairs with absolute distance smaller than - dmin are
    % considered as intersecting each other. This may cause troubles for pairs
    % with negative absolute distance greater than - dmin.
    % Besides the touching threshold dmin, we use another threshold value
    % 0.01 for the angle. If the difference between two unit vectors has a
    % norm less than 0.01, we treat the angle between the two vectors as 0.

    % @Date   : 2019-03-20 15:43
    % @Version: 0.8
    % @Change : Use Pov-ray to generate the view of the packing.
    %
    % @Date   : 2019-02-18 10:35
    % @Version: 0.7
    % @Change : 1. Move the unit tests and initial setups to a new class.
    % 2. Minor change to the stability check.

    % @Date   : 2019-01-19 15:37
    % @Version: 0.6
    % @Change : 1. Updated stability check. It still ends up in infinite loop
    % sometimes.

    % @Date   : 2018-10-30 09:45
    % @Version: 0.5
    % @Change : Use Monte Carlo method to estimate the void fractions for
    % quantiles.

    % @Date   : 2018-10-28 17:05
    % @Version: 0.4
    % @Change : 1. Fixed the infinite loop while rolling on two particles
    % and resting on three particles by backing one step.
    % 2. Use a sparse matrix for the adjacency matrix.
    % 3. More unit tests for various conditions.

    % @Date   : 2018-09-19 15:47
    % @Version: 0.3
    % @Change : Abandon the structs, and vectorize the calculation. The
    % particle positions and radii are stored in an array
    % `particles`, and the contacting particles are in a cell
    % array `contacts`.

    % @Date   : 2018-09-15 16:30
    % @Version: 0.2
    % @Change : Now only the near particles are searched when determining
    % potential hits. Total time is reduced by more than half.

    % @Date   : 2018-05-19 09:14
    % @Version: 0.1
    % @Change : Use a struct for each particle. Slow.

    %
    % @TODO    : Apply periodic boundaries so that the system can be expanded

    methods (Static)

        % ----- Position operations ----- %

        function Move(pid, pos)
            % MOVE Move a particle pid to a new position.

            global particles

            particles(pid, 1:3) = pos;
        end

        function Touch(pid1, pid2)
            % TOUCH Record the contacts for two touching particles.

            global contacts

            if pid2 == 0
                contacts{pid1} = unique([0, contacts{pid1}]);
            else
                contacts{pid1} = unique([contacts{pid1}, pid2]);
                contacts{pid2} = unique([contacts{pid2}, pid1]);
            end

        end

        function Detach(pid1, pid2)
            % DETACH Remove the contacts from detached particles.

            global contacts

            contacts{pid1} = contacts{pid1}(contacts{pid1} ~= pid2);
            contacts{pid2} = contacts{pid2}(contacts{pid2} ~= pid1);
        end

        function flag = Onfloor(pid)
            % ONFLOOR Determine if the particles can touch the floor
            % without hitting another.

            global particles

            if particles(pid, 3) <= particles(pid, 4)
                pos = particles(pid, 1:3);
                pos(:, 3) = particles(pid, 4);
                dnr.Move(pid, pos);
                dnr.Touch(pid, 0);
                flag = true;
            else
                flag = false;
            end

        end

        function flag = Falling(pid, checkstability)
            % FALLING Determine if the particle reaches the final rest
            % position. If check stability is enabled, a stability check is
            % performed.

            global contacts

            pcontact = contacts{pid};

            if ismember(0, pcontact)
                flag = false;
            elseif length(pcontact) == 3

                if checkstability
                    flag = ~ dnr.Stable(pid);
                else
                    flag = false;
                end

            else
                flag = true;
            end

        end

        % ----- Basic particle operations ----- %

        function d = Distance(pid1, pid2, attr)
            % DISTACE Find the horizontal or total distance between particles
            % p1 and p2. The attribute flag attr can be 'h' for horizontal
            % distance, or 't' for total distance. The default attribute is
            % 'total'.

            global particles

            % default total distance
            if nargin < 3
                attr = 't';
            end

            pos1 = particles(pid1, 1:3);
            pos2 = particles(pid2, 1:3);
            R1 = particles(pid1, 4);
            R2 = particles(pid2, 4);

            if attr == 'h'
                d = vecnorm(pos1(:, 1:2) - pos2(:, 1:2), 2, 2) - R1 - R2;
            else
                d = vecnorm(pos1 - pos2, 2, 2) - R1 - R2;
            end

        end

        function [hvec, phi] = Relative(pid, oid)
            % RELATIVE Calculate the azimuthal position of the sphere with
            % respect to the tangent sphere.

            global particles

            posdif = particles(pid, 1:3) - particles(oid, 1:3);
            hvec = posdif(1:2);

            if norm(hvec) < 0.01 % almost on top, use a different threshold?
                hvec = rand(1, 2);  % randomize the horizontal position
                % hvec = [- 1, 1]; % test
                phi = 0;
            else
                rsum = particles(pid, 4) + particles(oid, 4);
                zdif = posdif(3);
                phi = acos(zdif / rsum);
            end

            % horizontal relative position vector [cos(theta), sin(theta)]
            hvec = hvec / norm(hvec);
        end

        function apos = Absolute(oid, r, hvec, phi)
            % ABSOLUTE Calculate the absolute position of a particle based
            % on its relative position to the origin particle it rolls
            % around. This is the inverse of Relative.

            global particles

            pos = particles(oid, 1:3);
            rsum = r + particles(oid, 4);
            xy = rsum * sin(phi) * hvec + pos(1:2);
            z = rsum * cos(phi) + pos(3);
            apos = [xy, z];
        end

        function overlaplist = Overlap(pid)
            % OVERLAP Find the particles beneath the current one that
            % potentially overlaps.
            % Now overlaplist still has many particles, but the lower ones
            % do not have the chance to contact p, and will be sieved out in
            % Hit

            global particles

            xpos = particles(pid, 1);
            ypos = particles(pid, 2);
            r = particles(pid, 4);
            prevs = 1:pid - 1;
            xid = abs(particles(prevs, 1) - xpos) < r + particles(prevs, 4);
            yid = abs(particles(prevs, 2) - ypos) < r + particles(prevs, 4);
            overlaplist = prevs(xid & yid);
        end

        function touchlist = Around(pid)
            % AROUND Find the particles around the current one that touches
            % the current particle besides those already in the contacts list.

            global particles
            global contacts
            global dmin

            potential = 1:pid - 1;

            xpos = particles(pid, 1);
            ypos = particles(pid, 2);
            zpos = particles(pid, 3);
            r = particles(pid, 4);
            xid = abs(particles(potential, 1) - xpos) < r + particles(potential, 4);
            yid = abs(particles(potential, 2) - ypos) < r + particles(potential, 4);
            zid = abs(particles(potential, 3) - zpos) < r + particles(potential, 4);
            closeids = potential(xid & yid & zid);
            dists = dnr.Distance(pid, closeids);
            touchlist = setdiff(closeids(dists >- dmin & dists < 0), contacts{pid});
        end

        function nearlist = Near(pid, zr, Rr)
            % NEAR Find the particles within the sweep volume of the
            % rolling particle:
            % 1. rolling on one, rolling center is pos1, rolling radius
            % Rr = R1 + r;
            % 2. roling on two, rolling center is Q, rolling radius is Rr.
            % return particles satisfying zi + ri > zr - r and
            % zi < zr + Rr + r + ri.
            % Rolling plane may not be vertical.

            global particles

            r = particles(pid, 4);
            prevs = 1:pid - 1;
            highid = particles(prevs, 3) < zr + Rr + r + particles(prevs, 4);
            lowid = particles(prevs, 3) > zr - r - particles(prevs, 4);
            nearlist = prevs(highid & lowid);
        end

        % ----- Tangent particles ----- %

        function cuts = Hit(pid, potential, attr)
            % HIT Find previously dropped particles close to the current
            % on, and store the ids of intersecting particles in a list cuts.

            global particles
            global dmin


            % default total distance
            if nargin < 3
                attr = 't';
            end

            if ~ isempty(potential)
                z = particles(pid, 3);
                dists = zeros(size(potential));

                if attr == 'h'
                    % particles below the current close in horizontal distance
                    % ignore previous particles with higher centers
                    beneathid = particles(potential, 3) < z;
                    aboveid = particles(potential, 3) >= z;
                    dists(beneathid) = dnr.Distance(pid, ...
                        potential(beneathid), 'h');

                    if ~ isempty(aboveid)
                        dists(aboveid) = 1;
                    end

                else
                    dists = dnr.Distance(pid, potential);
                end

                cutmask = dists <- dmin;
                cutdist = dists(cutmask);
                cutids = potential(cutmask);
                % sort the distance by ascending order
                [~, sortIdx] = sort(cutdist, 'ascend');
                cuts = cutids(sortIdx);
            else
                cuts = [];
            end

        end

        function TangentOne(pid, cuts)
            % TANGENTONE Find dropped particles close beneath the dropping
            % one, and make it tangent to the nearest one.

            global particles

            r = particles(pid, 4);
            hpos = particles(pid, 1:2);
            zpos = sqrt((r + particles(cuts, 4)) .^ 2 ...
                - vecnorm(hpos - particles(cuts, 1:2), 2, 2) .^ 2) ...
                + particles(cuts, 3);
            [~, ind] = max(zpos);
            nearest = cuts(ind);
            pos = [hpos, zpos(ind)];
            dnr.Move(pid, pos);
            dnr.Touch(pid, nearest);
        end

        function TangentTwo(pid, pid1, pid2)
            % TANGENTTWO Adjust the position of p to contact with the
            % second one.
            % The rolling one is tangent with p1, and if it continues to
            % roll along the longitude, it will hit p2.

            global particles

            r = particles(pid, 4);
            [hvec, phi] = dnr.Relative(pid, pid1);

            f = @(phi) t2eq(r, hvec, phi);
            options = optimset('Display', 'off');
            newphi = fsolve(f, phi, options);
            dnr.Move(pid, dnr.Absolute(pid1, r, hvec, newphi));
            dnr.Touch(pid, pid2);

            function d = t2eq(r, hvec, phi) % touching two particles
                pos = dnr.Absolute(pid1, r, hvec, phi);
                dnr.Move(pid, pos);
                d = dnr.Distance(pid, pid2);
            end

        end

        function TangentThree(pid, pid1, pid2, pid3)
            % TANGENTTHREE Adjust the position to contact with three
            % particles. Or the Appolonius position. Much easier to
            % calculate the position by brute-force than geometric
            % calculations.

            global particles

            pos = particles(pid, 1:3);
            pos1 = particles(pid1, 1:3);
            pos2 = particles(pid2, 1:3);
            pos3 = particles(pid3, 1:3);

            % find the rest position
            options = optimset('Display', 'off');
            pt1 = fsolve(@t3eq, particles(pid, 1:3), options);
            % symmetric point is the other center
            n_vec = cross(pos2 - pos1, pos3 - pos1);  % normal vector
            n_vec = n_vec / norm(n_vec);
            pt_norm = dot(pt1 - pos1, n_vec) * n_vec;
            pt2 = pt1 - 2 * pt_norm;

            if norm(pos - pt1) < norm(pos - pt2)
                % it is tempting to select the higher solution, but in some
                % rare cases when two smaller particles rest on a huge
                % particle, the higher solution may exist on the
                % other side
                dnr.Move(pid, pt1);
            else
                dnr.Move(pid, pt2);
            end

            dnr.Touch(pid, pid3);

            function d = t3eq(pos) % touching three particles
                dnr.Move(pid, pos);
                d = dnr.Distance([pid1, pid2, pid3], pid);
            end

        end

        function flag = Stable(pid, ~)
            % STATBLE Check if the current particle in touch with three
            % other particles is stable, that is, the horizontal projection
            % of its center lies within the triangle formed by the centers of
            % the horizontal projection of three contacting particles.

            % If the particle is not stable, the particle should detach one
            % or two particles and continue rolling down. The stability check
            % is done via the barycentric method
            % https://stackoverflow.com/questions/2049582
            % To avoid infinite loops, we use a probability approach: with a
            % chance of 50%, the particle detaches the contacting particles
            % according to the regions of stability, with 25% chance it
            % randomly detaches one contact, and finally with 25% chance it
            % randomly detaches two contacts.
            % Still looking for a more straightforward and stable criterion.

            global particles
            global contacts

            pid1 = contacts{pid}(1);
            pid2 = contacts{pid}(2);
            pid3 = contacts{pid}(3);
            pos = particles(pid, 1:3);
            pos1 = particles(pid1, 1:3);
            pos2 = particles(pid2, 1:3);
            pos3 = particles(pid3, 1:3);

            if nargin > 1
                % plot the center and contacting points
                x = [pos1(1); pos2(1); pos3(1)];
                y = [pos1(2); pos2(2); pos3(2)];
                z = [pos1(3); pos2(3); pos3(3)];
                figure
                plot3(pos(1), pos(2), pos(3), 'r*', ...
                    [x; pos1(1)], [y; pos1(2)], [z; pos1(3)], '-+')
                view(3);
                text(x, y, z, {pid1, pid2, pid3})
            end

            % use the barycentric method to determine if the center is
            % inside the triangle

            A = - pos2(2) * pos3(1) + pos1(2) * (- pos2(1) + pos3(1)) ...
                + pos1(1) * (pos2(2) - pos3(2)) + pos2(1) * pos3(2);
            % we ignore the possibility that A = 0 due to collinear contacts
            s = 1 / A * (pos1(2) * pos3(1) - pos1(1) * pos3(2) ...
                + (pos3(2) - pos1(2)) * pos(1) + (pos1(1) - pos3(1)) * pos(2));
            t = 1 / A * (pos1(1) * pos2(2) - pos1(2) * pos2(1) ...
                + (pos1(2) - pos2(2)) * pos(1) + (pos2(1) - pos1(1)) * pos(2));

            flag = (s >= 0 || abs(s) < 0.001) ...
                && (t >= 0 || abs(t) < 0.001) ...
                && (s + t <= 1 || abs(s + t - 1) < 0.001);

            % detach the contact if unstable
            if ~ flag
                check = rand;

                if check < 0.5
                    % regions of stability
                    if (s > 0) && (t > 0) && (s + t >= 1)
                        dnr.Detach(pid, pid1);
                    elseif (s > 0) && (t <= 0) && (s + t < 1)
                        dnr.Detach(pid, pid3);
                    elseif (s <= 0) && (t > 0) && (s + t < 1)
                        dnr.Detach(pid, pid2);
                    elseif (s <= 0) && (t <= 0)
                        dnr.Detach(pid, pid2);
                        dnr.Detach(pid, pid3);
                    elseif (s <= 0) && (s + t >= 1)
                        dnr.Detach(pid, pid1);
                        dnr.Detach(pid, pid2);
                    elseif (t <= 0) && (s + t >= 1)
                        dnr.Detach(pid, pid1);
                        dnr.Detach(pid, pid3);
                    end

                elseif check < 0.75
                    % randomly detach one contacts with 25% chance
                    iz = randi([1, 3]);
                    dnr.Detach(pid, contacts{pid}(iz));
                else % randomly detach two contacts with 25% chance
                    iz2 = sort(randsample(3, 2));
                    % first detach the one with larger index
                    dnr.Detach(pid, contacts{pid}(iz2(2)));
                    dnr.Detach(pid, contacts{pid}(iz2(1)));
                end

            end

        end

        % ----- Rolling particles ----- %

        function RollOne(pid, pid1)
            % ROLLONE Roll the current particle on the tangent one, until
            % it hits another, or touches the floor, or detaches the first
            % one.

            global particles
            global dmin

            % p and p1 are tangent
            [hvec, phi] = dnr.Relative(pid, pid1);
            r = particles(pid, 4);
            rsum = particles(pid1, 4) + r;
            zr = particles(pid1, 3);
            % rolling down by increasing phi
            % after one move, one or more particles will intersect with the
            % rolling one
            incMax = 50;
            inc = 0;

            if zr > r
                adj = (pi / 2 - phi) / incMax;
            else
                adj = (acos((r - zr) / rsum) - phi) / incMax;
            end

            while inc <= incMax - 1
                % record current position
                oldpos = particles(pid, 1:3);
                phi = phi + adj;
                inc = inc + 1;
                pos = dnr.Absolute(pid1, r, hvec, phi);
                dnr.Move(pid, pos);
                nearlist = dnr.Near(pid, zr, rsum);
                % search only close ones
                cuts = dnr.Hit(pid, nearlist);

                if ~ isempty(cuts)
                    % hit another particle while rolling
                    % go to the position touching the closest
                    % there's a possibility that new cuts occur during
                    % the adjusting, which is corrected iteratively
                    while ~ isempty(cuts)
                        pid2 = cuts(1);
                        % back one step
                        dnr.Move(pid, oldpos);
                        dnr.TangentTwo(pid, pid1, pid2);
                        % reuse nearlist
                        cuts = dnr.Hit(pid, nearlist);

                        if ~ isempty(cuts)
                            dnr.Detach(pid, pid2)
                        end

                    end

                    break % out of loop with two particle contacts
                elseif inc == incMax

                    if abs(pos(3) - r) < abs(dmin)
                        % no cuts but near the floor, only when r > R1
                        dnr.Touch(pid, 0);  % settled on the floor
                    else
                        % at the equator, detaching
                        dnr.Detach(pid, pid1);
                    end

                    break % out of loop without contacts
                end

            end

        end

        function RollTwo(pid, pid1, pid2)
            % ROLLTWO Roll on two particles p1 and p2, and p1 is above p2.
            % Geometrically, the trajectory of the center of the rolling
            % particle can be described using parametric equations with
            % variable psi, and we increment this psi until the particle
            % hits a third one.

            global particles
            global dmin

            r = particles(pid, 4);
            pos1 = particles(pid1, 1:3);
            pos2 = particles(pid2, 1:3);
            rsum = particles(pid1, 4) + r;
            vec12 = pos2 - pos1;
            d12 = norm(vec12);  % >= R1 + R2
            vec_l = vec12 / d12;
            % if p1 is almost on the top of p2, which should be rare and
            % only happen when Rp >(R1 + R2)^2/2/(R2 - R1)
            if norm(vec_l(1:2)) < 0.01
                dnr.Detach(pid, pid1);  % detach the higher one
            else
                vec1p = particles(pid, 1:3) - pos1;
                vec_p = vec1p / rsum;
                % the rotation radius
                Rr = norm(cross(vec12, vec1p)) / d12;
                % the center of rotation
                posr = rsum * dot(vec_l, vec_p) * vec_l + pos1;
                zr = posr(3);
                % vectors of the rolling plane
                z_hat = [0, 0, 1];
                vec_n1 = cross(vec_l, z_hat);
                vec_n1 = vec_n1 / norm(vec_n1);
                vec_n2 = cross(vec_l, vec_p);
                vec_n2 = vec_n2 / norm(vec_n2);
                vec_t = cross(vec_n1, vec_l);
                vec_t = vec_t / norm(vec_t);

                if norm(vec_n1 - vec_n2) < 0.01 % almost on top
                    psi = 0;
                else
                    psi = acos(dot(vec_n1, vec_n2));
                end

                if abs(psi - pi / 2) < 0.01 % almost detach
                    dnr.Detach(pid, pid1);  % detach the higher one
                else
                    planedir = dot(vec_n1, vec_p);

                    if abs(planedir) < 0.01
                        % f = - 1; % test
                        f = randsample([- 1, 1], 1);  % go either direction
                    else
                        f = sign(dot(z_hat, vec_n2));
                    end

                    % rolling down by increasing psi
                    incMax = 50;
                    inc = 0;

                    if zr > r
                        adj = (pi / 2 - psi) / incMax;
                    else
                        adj = (acos((r - zr) / Rr / dot(vec_t, z_hat)) - psi) ...
                            / incMax;
                    end

                    while inc <= incMax - 1
                        % record current position
                        oldpos = particles(pid, 1:3);
                        psi = psi + adj;
                        inc = inc + 1;
                        vec_r = cos(psi) * vec_t - f * vec_n1 * sin(psi);
                        vec_r = vec_r / norm(vec_r) * Rr;
                        pos = vec_r + posr;
                        dnr.Move(pid, pos);
                        nearlist = dnr.Near(pid, posr(3), Rr);
                        cuts = dnr.Hit(pid, nearlist);

                        if ~ isempty(cuts) % hit another particle

                            while ~ isempty(cuts)
                                pid3 = cuts(1);
                                % back one step
                                dnr.Move(pid, oldpos);
                                dnr.TangentThree(pid, pid1, pid2, pid3);
                                cuts = dnr.Hit(pid, nearlist);

                                if ~ isempty(cuts)
                                    dnr.Detach(pid, pid3);
                                end

                            end

                            break
                            % out of loop resting on three contacts
                        elseif inc == incMax

                            if abs(pos(3) - r) < abs(dmin)
                                % no cuts but near the floor (large r)
                                dnr.Touch(pid, 0);  % settled on the floor
                            else
                                % go to equator without intersection, detaching
                                dnr.Detach(pid, pid1);
                                dnr.Detach(pid, pid2);
                                break % out of loop without contacts
                            end

                        end

                    end

                end

            end

        end

        % ----- Debugging ----- %

        function Remove(pid)
            % REMOVE A debug function to reset the particle pid.

            global particles
            global contacts
            global initial

            particles(pid, 1:3) = initial(pid, 1:3);
            clist = contacts{pid};
            clist = clist(clist ~= 0);

            if ~ isempty(clist)

                for i = 1:length(clist)
                    dnr.Detach(pid, clist(i));
                end

            end

            contacts{pid} = [];
        end

        function Balance(pid)
            % BALANCE Plot the projects of centers of particle pid (red)
            % and particles around.

            global particles
            global contacts

            pos = particles(pid, 1:3);

            pids = [contacts{pid}, dnr.Around(pid)];
            x = particles(pids, 1);
            y = particles(pids, 2);
            z = particles(pids, 3);

            % plot the centers

            figure
            plot3(pos(1), pos(2), pos(3), 'r*', x, y, z, '+')
            view(3);
            text(x, y, z, num2cell(pids, 1))

        end

        function Intersect(pid)
            % INTERSECT A debug function to find all intersecting particles.
            %
            % This is faster than the following implementation:
            % pair=combnk(1:pid,2); % much more costly for large pid
            % cutIdx= Distance(pair(:,1), pair(:,2)) < - dmin;
            % intersection = pair(cutIdx,:);

            global dmin

            intersection = [];

            for i = 1:pid
                js = i + 1:pid;
                cutIdx = dnr.Distance(i, js) <- dmin;
                pair = [repmat(i, [pid - i, 1]), js'];
                intersection = [intersection; pair(cutIdx, :)];
            end

            if isempty(intersection)
                disp('No intersections found in previous particles!')
            else
                disp('Intersections found:')
                disp(intersection);
            end

        end

        % ----- Analysis ----- %

        function ztop = Top(particles, pid)
            % TOP Find the highest particle for particles up to pid.

            if nargin < 2
                pid = size(particles, 1);
            end

            zpos = particles(1:pid, 3);
            [ztop, ind] = max(zpos);
            xyz = particles(ind, 1:3);
            fprintf('The top of first %d is particle %d at [%2.2f, %2.2f, %2.2f]\n', ...
                pid, ind, xyz(1), xyz(2), xyz(3))
        end

        function [sigma, cv] = Variance(particles)
            % VARIANCE Calculate the log-normal standard deviation and variation
            % coefficient of particles following log-normal size distribution.

            r = particles(:, 4);
            m = mean(r);
            v = var(r);
            cv = sqrt(v) / m;
            sigma = sqrt(log(1 + cv ^ 2));
        end

        function Visualize(pids)
            % VISUALIZE Plot particles with specific ids.

            global particles
            global contacts

            if length(pids) == 1
                cids = contacts{pids}(contacts{pids} ~= 0);
                pids = [pids, cids];
            end

            px = particles(pids, 1);
            py = particles(pids, 2);
            pz = particles(pids, 3);
            pr = particles(pids, 4);
            figure
            bubbleplot3(px, py, pz, pr)
            shading interp;
            camlight right;
            lighting phong;
            view(60, 30);
        end

        function Povray(sedfile, name)
            % POVRAY Generate a pov-ray input file from sedfile, and Rv is
            % the viewing radius.

            if nargin < 2
                name = 'poly';
            end

            p = getfield(load(sedfile), 'particles');

            rsize = range(p(:, 1));
            Rv = 2 * rsize;

            % view point
            az = - 60;  % azimuth angle
            el = 30;  % elevation angle
            L = range(p(:, 1));
            M = mean(p(:, 3));  % look at the middle of the packing
            % radius of the view
            vr = Rv * ceil(L / Rv);

            pos = vr * [cosd(el) * sind(az), cosd(el) * cosd(az), sind(el)];
            fid = fopen([name, '.pov'], 'w');
            tab = blanks(4);
            fprintf(fid, '#include "colors.inc"\n');
            fprintf(fid, '#include "shapes.inc"\n');
            fprintf(fid, '#include "textures.inc"\n');
            fprintf(fid, '#include "finish.inc"\n\n');
            fprintf(fid, 'global_settings {\n');
            fprintf(fid, [tab, 'assumed_gamma 1.0\n']);
            fprintf(fid, '}\n\n');
            fprintf(fid, 'background{White}\n');
            fprintf(fid, 'camera {\n');
            fprintf(fid, [tab, 'perspective\n']);
            fprintf(fid, [tab, 'location <%d, %d, %d>\n'], pos);
            fprintf(fid, [tab, 'look_at <0, %f, 0>\n'], M);
            fprintf(fid, '}\n\n');
            fprintf(fid, '#declare sph=union{\n');

            for i = 1:size(p, 1)
                % Pov-ray has +x pointing to the right, +y pointing up, and
                % +z pointing into the screen
                s = [p(i, 2), p(i, 3), p(i, 1), p(i, 4)];
                fprintf(fid, [tab, 'sphere{<%.4f, %.4f, %.4f>, %.4f}\n'], s);
            end

            fprintf(fid, '}\n\n');
            fprintf(fid, 'object{sph\n');
            fprintf(fid, [tab, 'texture{\n']);
            fprintf(fid, [tab, tab, 'pigment{Pink}\n']);
            fprintf(fid, [tab, tab, 'finish{ambient 0.2 diffuse 0.4 specular 1}\n']);
            fprintf(fid, [tab, '}\n']);
            fprintf(fid, '}\n\n');
            fprintf(fid, 'light_source {<%d, %d, %d> color White}', pos);

            disp('Povray file sucessfully generated!')
            disp('Run pov-ray with:')
            disp(['povray -i', name, '.pov +fp +a +h2400 +w3200 -o- | cjpeg >', ...
                    name, '.png'])
        end

        function Adjacency(contacts, msize)
            % ADJACENCY Parse the contact lists into a symmetric matrix.
            % For particle i with contacts j1, j2, ..., jk, the matrix has
            % element 1 at(i, j1), (i, j2), ..., (i, jk). With 0 as an
            % additional node, the matrix for n particles has a size
            % (n + 1) *(n + 1).

            if nargin < 2
                msize = 10;
            end

            n = length(contacts);
            cmatrix = sparse(n + 1, n + 1);

            for row = 1:n
                col = contacts{row} + 1;
                cmatrix(row + 1, col) = 1;
            end

            cmatrix(1, :) = transpose(cmatrix(:, 1));
            spy(cmatrix, msize);
        end

        function Xsection(particles, z)
            % XSECTION Draw a horizontal cross-sections showing the particles at
            % height z.

            zpos = particles(:, 3);
            r = particles(:, 4);
            hpos = particles(:, 1:2);
            teal = [0, 0.5, 0.5];

            figure
            zid = abs(zpos - z) <= r;
            dz = zpos(zid) - z;
            zr = sqrt(r(zid) .^ 2 - dz .^ 2);
            zhpos = hpos(zid, :);
            title(['z = ', num2str(z), ' $\mu$m'], 'interpreter', 'latex')

            viscircles(zhpos, zr, 'Color', teal);
            axis equal
        end

        function count = Occupy(particles, pos)
            % OCUPPY Find if a points (x, y, z) is inside any particles.

            pids = 1:size(particles, 1);
            % first a rough check
            xbound = abs(particles(:, 1) - pos(1)) < particles(:, 4);
            ybound = abs(particles(:, 2) - pos(2)) < particles(:, 4);
            zbound = abs(particles(:, 3) - pos(3)) < particles(:, 4);
            nears = pids(xbound & ybound & zbound);

            if isempty(nears)
                count = 0;
            else
                % then a fine check
                mask = vecnorm(particles(nears, 1:3) - pos, 2, 2) ...
                    < particles(nears, 4);

                if sum(mask) > 0
                    count = 1;
                else
                    count = 0;
                end

            end

        end

        function vf = Void(particles, nq, num)
            % VOID Calculate the void fractions of the particle deposit in
            % quantiles (default quartiles). If the height range is not
            % specified, the void fractions in each of the four quartiles
            % are calculated using the Monte Carlo method.

            zpos = particles(:, 3);

            if nargin < 3
                num = 10000;  % number of random points
            end

            if nargin < 2
                nq = 4;
            end

            L = range(particles(:, 1)) / 3;

            % divide the region into quantiles
            zq = [min(zpos), quantile(zpos, 1 / nq:1 / nq:1)];
            hq = diff(zq);
            vf = zeros(nq, 1);
            % calculate the void fractions of the center in each quantile
            for i = 1:nq
                pos = [L * (rand(num, 2) - 0.5) / 2, zq(i) + hq(i) * ones(num, 1)];
                C = zeros(num, 1);

                for j = 1:num
                    C(j) = dnr.Occupy(particles, pos(j, :));
                end

                vf(i) = 1 - sum(C) / num;
            end

        end

    end

end
