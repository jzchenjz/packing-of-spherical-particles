classdef drop
    % This contains the unit tests, initial setups for the Drop-aNd-Roll
    % simulations, and functions to run a simulation, or restart a
    % simulation terminated by the user.
    %
    % Random loose packing of unequal hard particles is based on Jodrey & Tory
    % (1979), and incorporated algorithms by Julia and Alan.

    % Each particles has four essential elements: ID, center position, radius,
    % and a list of pid of the contacting particles. If a particles touches
    % the floor, it has a special ID 0 in its contacts list. We use a matrix
    % 'particles' for the positions and radii, and a cell array 'contacts' for
    % the contacts. See dnr.m for details.

    % @Date : 2019-02-18 10:35
    % @Version : 0.2
    % @Change : Add terminate/resume functionalities. When the user
    % terminates the simulation, the workspace is automatically
    % saved to a restart file, which can be loaded when the
    % simulation is restarted to resume the simulation.

    % @Date : 2019-01-19 15:37
    % @Version : 0.1
    % @Change : Move the unit tests and initial setups to a separate class.

    methods (Static)

        % ----- Unit tests ----- %

        function [particles, contacts, initial] = Test1
            % TEST1 A test setup of equal-sized particles.
            %
            % One fixed particle p1 with a radius of 1 is at (0, 0, 1), and
            % a new particle p2 with a radius of 1 is dropping from (0, 0, 99).
            % It should settle on the side of p1, and the contact position is
            % right at the equator of p1.

            particles = [0, 0, 1, 1; 0, 0, 99, 1];

            contacts{1} = 0;
            contacts{2} = [];

            initial = particles; % store original values
        end

        function [particles, contacts, initial] = Test2
            % TEST2 A test setup for particles to detach correctly.
            %
            % One fixed particle p1 with a radius of 2 is at (0, 0, 2), and
            % another fixed p2 with a radius of 1 is at(0, 0, 5). A new
            % particle p3 with a radius of 5 is settling from (0, 0, 99).
            % It should first hit p2, roll on p2, touch p1, detach from p2,
            % and continue to roll down on p1 and finally reach the floor.

            particles = [0, 0, 2, 2; 0, 0, 5, 1; 0, 0, 99, 5];

            contacts{1} = 0;
            contacts{2} = 0;
            contacts{3} = [];

            initial = particles; % store original values
        end

        function [particles, contacts, initial] = Test3
            % TEST3 A test setup for rolling down two particles.
            %
            % One fixed particle p1 with a radius of 2 is at (0, 0, 2), and
            % another p2 with a radius of 4 is at (6, 0, 4). A new particle
            % p3 with a radius of 6 is dropping from (2, 1, 99). It should
            % contact the two particles and roll down them and reach the
            % floor.

            particles = [0, 0, 2, 2; 6, 0, 4, 4; 2, 1, 99, 6];

            contacts{1} = 0;
            contacts{2} = 0;
            contacts{3} = [];

            initial = particles; % store original values
        end

        function [particles, contacts, initial] = Test4
            % TEST4 A test setup of big and small particles.
            %
            % Three fixed particles with a radius 1.5 are at (1.5, 0, 1.5),
            % (-1.5, 0, 1.5) and (0, 1.5 * tand(30), 1.5), and a smaller
            % particle p4 with a radius 0.23 is dropping from (0, 0.5, 99),
            % and a slightly larger p5 with a radius 0.24 drops from the
            % same position. The particles p4 should go through the hole
            % but p5 should not.

            particles = [1.5, 0, 1.5, 1.5; ...
                        - 1.5, 0, 1.5, 1.5; 0, ...
                        1.5 * tand(60), 1.5, 1.5; ...
                        0, 0.5, 99, 0.23; ...
                        0, 0.5, 99, 0.24];

            contacts{1} = 0;
            contacts{2} = 0;
            contacts{3} = 0;
            contacts{4} = [];
            contacts{5} = [];

            initial = particles; % store original values
        end

        % ----- Initialization ----- %

        function [particles, contacts, initial] = Mono(n, L)
            % MONO Initialize mono-dispersed particles.
            %
            % The particles have uniform random starting (x, y) positions
            % with a height 999999 with a constant radius.

            % Constants
            if nargin < 2
                L = 100;

                if nargin < 1
                    n = 1000;
                end

            end

            dropz = 999999;

            pos = [L * (rand(n, 2) - 0.5), dropz * ones(n, 1)];
            radius = ones(n, 1);

            particles = [pos, radius];

            contacts = cell([n, 1]);
            initial = particles; % store original values
        end

        function [particles, contacts, initial] = Lognormal(n, L, mu, sigma)
            % LOGNORMAL Initialize poly-dispersed particles according to a
            % pre-defined log-normal distribution.
            %
            % The particles have uniform random starting (x, y) positions with
            % a height 999999. The radii of the particles follow a log-normal
            % distribution.

            % Constants
            if nargin < 3
                % Log-normal distribution for jsc-mars-1
                mu = 3.19;
                sigma = 0.75;
            end

            dropz = 999999;

            pos = [L * (rand(n, 2) - 0.5), dropz * ones(n, 1)];
            radius = lognrnd(mu, sigma, n, 1);

            particles = [pos, radius];

            contacts = cell([n, 1]);
            initial = particles; % store original values
        end

        function [particles, contacts, initial] = Bimodal(n, L, f, mu, sigma)
            % BIMODAL Initialize poly-dispersed particles following a bimodal
            % distribution by mixing two log-normal distributions. The fraction
            % of the first distribution is f.

            n1 = round(n * f);
            [p1, c1, ~] = drop.Lognormal(n1, L, mu(1), sigma(1));
            [p2, c2, ~] = drop.Lognormal(n - n1, L, mu(2), sigma(2));

            particles = zeros(n, 4);
            contacts = cell(n, 1);
            idx1 = randsample(n, n1);
            idx2 = setdiff((1:n)', idx1);
            particles(idx1, :) = p1;
            particles(idx2, :) = p2;
            contacts(idx1) = c1;
            contacts(idx2) = c2;
            initial = particles; % store original values
        end

        % ----- Simulation and restart ----- %

        function run(startid)
            % RUN Main settling loop.
            % Get one particle, and use the contact list to determine whether
            % the current particle can roll down more.
            % If the simulation is interrupted by the user, a restart file
            % is automatically generated in the current folder.

            cleanupObj = onCleanup(@saveStatus);

            global particles
            global contacts
            global initial

            global dmin
            dmin = min(initial(:, 4)) / 100; % touching threshold

            if nargin < 1
                startid = 1;
            end

            checkstability = 0; % check stability as a default

            n = size(particles, 1);

            for pid = startid:n

                if mod(pid, 50) == 0
                    disp(['Settled particles: ', num2str(pid)]);
                end

                % iterate until touching floor or resting on three particles
                while dnr.Falling(pid, checkstability)

                    if isempty(contacts{pid}) % no contacts
                        overlaplist = dnr.Overlap(pid);
                        cuts = dnr.Hit(pid, overlaplist, 'h');

                        if isempty(cuts) % touch floor in the beginning
                            particles(pid, 3) = particles(pid, 4);
                            dnr.Touch(pid, 0);
                            break
                        else
                            dnr.TangentOne(pid, cuts);
                        end

                    end

                    if length(contacts{pid}) == 1
                        pid1 = contacts{pid};
                        % there is a possibility that p has a large radius
                        % that it does not contact p1
                        if dnr.Onfloor(pid)
                            dnr.Detach(pid, pid1);
                        else
                            dnr.RollOne(pid, pid1);
                        end

                        if ~ dnr.Falling(pid, checkstability)
                            break
                        end

                    end

                    % still falling, hit or detach
                    if length(contacts{pid}) == 2
                        % hit a second particle
                        pid1 = contacts{pid}(1);
                        pid2 = contacts{pid}(2);

                        if particles(pid1, 3) > particles(pid2, 3)
                            % ensure that p1 is higher
                            dnr.RollTwo(pid, pid1, pid2);

                        else
                            dnr.RollTwo(pid, pid2, pid1);
                        end

                        if ~ dnr.Falling(pid, checkstability)
                            break
                        end

                    end

                end

            end

            function saveStatus
                % SAVESTATUS Save the variables upon keyboard termination for
                % resuming the simulation.

                if pid < n
                    warning('Terminated by user, workspace saved.')
                    filename = ['restart-', ...
                                datestr(now, 'mm-dd-yyyy-HH-MM'), '.mat'];
                    save(filename, 'particles', 'contacts', 'initial', 'pid')
                else
                    disp('Simulation completed!')

                    if var(initial(:, 4)) % mono- or poly-dispersed particles
                        filename = ['poly-', num2str(n), ...
                                    datestr(now, '-mm-dd-yyyy-HH-MM'), '.mat'];
                    else
                        filename = ['mono-', num2str(n), ...
                                    datestr(now, '-mm-dd-yyyy-HH-MM'), '.mat'];
                    end

                    save(filename, 'particles', 'contacts', 'initial')
                end

            end

        end

        function restart(restartfile)
            % RESTART Resume a terminated simulation.

            load(restartfile)
            drop.run(pid);
        end

    end

end
