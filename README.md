# packing of spherical particles

Matlab scripts to simulate the packing of spherical particles using the Jodrey-Tory methods. The functions are organized in one class `dnr.m`. The tests and initial setups are in `drop.m`. The script `run_drop.m` runs a simulation.

