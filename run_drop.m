% Constants
clear
n = 50000;

% Initialize particles with an ID, center position, radius, and a list of
% the ids of contacting particles.

global particles
global contacts
global initial

% L = 100 * exp(3 / 2 * s ^ 2);
%
% [particles, contacts, initial] = drop.Lognormal(n, L, 0, s);
% drop.run

% [particles, contacts, initial] = drop.Bimodal(n, L, 0.5, [1, 3], [0.5, 0.5]);
%
L = 100 * exp(3 / 2 * 0.6 ^ 2);
[particles, contacts, initial] = drop.Bimodal(n, L, 0.5, [0, 0], [0.3, 0.6]);
 

drop.run

% Occasionally the simulation gets stuck in an infinite loop. In this case,
% we can terminate the simulation, and a restart file is automatically
% saved in the current folder. We can either hunt for the bug that causes
% the loop, or just skip the particle manually and move on.

% after pressing ctrl-c
% clc
% load restart-xx-xx-xxxx-xx-xx.mat
% check the nearby particles
% dnr.Balance(pid)
% add the nearby particles to contacts list
% for i = 1:length(dnr.Around(pid))
% dnr.Touch(pid, dnr.Around(pid)(i));
% end
% pid = pid + 1;
% save('p')
% drop.restart('p.mat')
